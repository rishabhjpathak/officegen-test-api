// const fs = require("fs");
const officegen = require("officegen");
const { LOGO, HEADER } = require("./constants");

const downloadDocx = async (req, res) => {
  try {
    const dataAsJson = req.body;

    const docx = officegen({
      type: "docx",
      orientation: "portrait",
      pageMargins: {
        top: 400,
        left: 400,
        bottom: 400,
        right: 400,
      },
    });
    docx.on("error", (e) => {
      throw e;
    });

    docx.on("error", (e) => console.log(e));
    docx.createByJson(dataAsJson);
    setHeaderAndFooter(docx);

    return docx.generate(res);
  } catch (error) {
    console.log(error);
    return res.json({ error: true, success: false, message: error.message });
  }
};

const downloadXlsx = async (req, res) => {
  try {
    const { body } = req;

    const xlsx = officegen({
      type: "xlsx",
    });
    xlsx.on("error", (e) => {
      throw e;
    });

    for (let item of body) {
      const { data, headers, widthArray, sheetName } = item;
      const sheet = xlsx.makeNewSheet();
      sheet.name = sheetName;
      sheet.width = widthArray;
      for (let i = 0; i < data.length; i++) {
        const row = data[i];
        for (let j = 0; j < headers.length; j++) {
          const cell = i === 0 ? headers[j] : row[j];
          sheet.setCell(`${toLetters(j + 1)}${i + 1}`, cell);
        }
      }
    }
    return xlsx.generate(res);
  } catch (error) {
    console.log(error);
    return res.json({ error: true, success: false, message: error.message });
  }
};

const setHeaderAndFooter = (docx) => {
  const header = docx.getHeader().createP();
  // header.addImage(LOGO, { cy: 84.661417323, cx: 114.8976378 });
  header.addImage(HEADER, { cy: 100, cx: 760 });

  // TODO
  // align this on the right side of the document's header
  // header.addText(
  //   "Fefifo Malaysia Sdn Bhd\n64-3, Jalan 27/70a\nDesa Sri Hartamas\n50480 Kuala Lumpur, Malaysia\nCompany No. 834529-T"
  // );
  const footer = docx.getFooter().createP();
  // footer.addText("Fefifo Malaysia Sdn Bhd");
  footer.addTextLR({ l: "Fefifo Malaysia Sdn Bhd", r: "ABCD" });
  return docx;
};

function toLetters(num) {
  var mod = num % 26,
    pow = (num / 26) | 0,
    out = mod ? String.fromCharCode(64 + mod) : (--pow, "Z");
  return pow ? toLetters(pow) + out : out;
}

module.exports = { downloadDocx, downloadXlsx };
