const path = require("path");

exports.LOGO = path.resolve(__dirname, "../public/images/logo.png");
exports.HEADER = path.resolve(__dirname, "../public/images/header.png");
